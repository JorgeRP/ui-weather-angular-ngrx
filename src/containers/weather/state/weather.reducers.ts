import { SEARCH_SUCCESS, SEARCH_ERROR } from "./weather.actions";
const initialState = {};

export function reducer(state = initialState, action) {
  switch (action.type) {
    case SEARCH_SUCCESS:
      return { ...state, weatherData: action.payload };
    case SEARCH_ERROR:
      return { ...state, weatherDataError: action.payload };
    default:
      return state;
  }
}
