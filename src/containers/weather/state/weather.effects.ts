import { SEARCH, SEARCH_SUCCESS, SEARCH_ERROR } from "./weather.actions";
import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects/";
import { switchMap, map, catchError } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { of } from "rxjs/internal/observable/of";

@Injectable()
export class WeatherEffects {
  constructor(private action$: Actions, private http: HttpClient) {}

  @Effect()
  search$ = this.action$.pipe(
    ofType(SEARCH),
    switchMap(action => {
      return this.http
        .get(
          `http://api.openweathermap.org/data/2.5/weather?appid=e19f346caeea76c58c647db73822fc3d&q=${
            action["payload"]
          }&units=metric`
        )
        .pipe(
          map(data => {
            return {
              payload: data,
              type: SEARCH_SUCCESS
            };
          }),
          catchError(err => {
            return of({
              payload: err,
              type: SEARCH_ERROR
            });
          })
        );
    })
  );
}
