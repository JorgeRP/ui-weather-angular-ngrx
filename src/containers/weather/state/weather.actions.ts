import { Action } from "@ngrx/store";
import { createActionType } from "src/shared/utils";

export const SEARCH = createActionType("SEARCH");
export const SEARCH_SUCCESS = createActionType("SEARCH_SUCCESS");
export const SEARCH_ERROR = createActionType("SEARCH_ERROR");

export class Search implements Action {
  readonly type = SEARCH;

  constructor(public payload) {}
}

export class SearchSuccess implements Action {
  readonly type = SEARCH_SUCCESS;
}

export class SearchError implements Action {
  readonly type = SEARCH_ERROR;
}
