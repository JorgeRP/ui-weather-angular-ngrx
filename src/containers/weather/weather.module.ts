import { ComponentsModule } from "./../../components/components.module";
import { environment } from "./../../environments/environment.prod";
import { WeatherEffects } from "./state/weather.effects";
import { reducer } from "./state/weather.reducers";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { WeatherRoutingModule } from "./weather-routing.module";
import { WeatherComponent } from "./weather.component";
import { StoreModule } from "@ngrx/store";
import { StoreRouterConnectingModule } from "@ngrx/router-store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { EffectsModule } from "@ngrx/effects";
import { routerReducer } from "@ngrx/router-store";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    WeatherRoutingModule,
    StoreModule.forFeature("weatherStore", reducer),
    EffectsModule.forFeature([WeatherEffects]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    ComponentsModule
  ],
  declarations: [WeatherComponent]
})
export class WeatherModule {}
