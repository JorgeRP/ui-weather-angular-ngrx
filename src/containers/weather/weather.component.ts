import { Search } from "./state/weather.actions";
import { Component, OnInit } from "@angular/core";
import { Store, createFeatureSelector, createSelector } from "@ngrx/store";

@Component({
  selector: "app-weather",
  templateUrl: "./weather.component.html",
  styleUrls: ["./weather.component.css"]
})
export class WeatherComponent implements OnInit {
  searchValue;
  data;
  errorData;
  weather;
  error;
  getWeatherStore = createFeatureSelector("weatherStore");
  getWeatherData = createSelector(
    this.getWeatherStore,
    state => state["weatherData"]
  );
  getWeatherDataError = createSelector(
    this.getWeatherStore,
    state => state["weatherDataError"]
  );
  constructor(private store: Store<any>) {}

  ngOnInit() {
    this.data = this.store.select(this.getWeatherData);
    this.errorData = this.store.select(this.getWeatherDataError);
    this.data.subscribe(data => {
      this.weather = data;
    });
    this.errorData.subscribe(err => {
      this.error = err;
      console.log(this.error);
    });
  }

  searchChange(event) {
    this.store.dispatch(new Search(event));
  }
}
