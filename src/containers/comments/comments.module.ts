import { reducer } from "./state/comments.reducers";
import { environment } from "./../../environments/environment";
import { CommentsEffects } from "./state/comments.effects";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CommentsRoutingModule } from "./comments-routing.module";
import { CommentsComponent } from "./comments.component";
import { StoreModule } from "@ngrx/store";
import { StoreRouterConnectingModule } from "@ngrx/router-store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { EffectsModule } from "@ngrx/effects";
import { routerReducer } from "@ngrx/router-store";
import { ComponentsModule } from "src/components/components.module";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    CommentsRoutingModule,
    StoreModule.forFeature("commentsStore", reducer),
    EffectsModule.forRoot([CommentsEffects]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    ComponentsModule
  ],
  declarations: [CommentsComponent]
})
export class CommentsModule {}
