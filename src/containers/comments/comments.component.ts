import { GetComments } from "./state/comments.actions";
import { Component, OnInit } from "@angular/core";
import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Store } from "@ngrx/store";

@Component({
  selector: "app-comments",
  templateUrl: "./comments.component.html",
  styleUrls: ["./comments.component.css"]
})
export class CommentsComponent implements OnInit {
  getCommentsStore = createFeatureSelector("commentsStore");
  getCommentsData = createSelector(
    this.getCommentsStore,
    state => state["comments"]
  );
  comments;
  data;
  constructor(private store: Store<any>) {}

  ngOnInit() {
    this.data = this.store.select(this.getCommentsData);
    this.data.subscribe(data => {
      this.comments = data;
    });
    this.store.dispatch(new GetComments());
  }
}
