import { switchMap, map, catchError } from "rxjs/operators";
import {
  GET_COMMENTS,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_ERROR
} from "./comments.actions";
import { Injectable } from "@angular/core";
import { Actions, Effect } from "@ngrx/effects/";
import { ofType } from "@ngrx/effects";
import { HttpClient } from "@angular/common/http";
import { of } from "rxjs/internal/observable/of";

@Injectable()
export class CommentsEffects {
  URL_HEROKU_API = "http://reduxblog.herokuapp.com/";
  API_KEY = "afe3ee0c3173e1890785f28e032102a";
  ENDPOINTS = {
    POSTS: "api/posts"
  };
  constructor(private action$: Actions, private http: HttpClient) {}

  @Effect()
  getComments$ = this.action$.pipe(
    ofType(GET_COMMENTS),
    switchMap(action => {
      return this.http
        .get(
          `${this.URL_HEROKU_API}${this.ENDPOINTS.POSTS}?key=${this.API_KEY}`
        )
        .pipe(
          map(data => {
            return {
              payload: data,
              type: GET_COMMENTS_SUCCESS
            };
          }),
          catchError(err => {
            return of({
              payload: err,
              type: GET_COMMENTS_ERROR
            });
          })
        );
    })
  );
}
