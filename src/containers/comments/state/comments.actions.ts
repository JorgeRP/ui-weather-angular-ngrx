import { Action } from "@ngrx/store";
import { createActionType } from "src/shared/utils";

export const GET_COMMENTS = createActionType("GET_COMMENTS");
export const GET_COMMENTS_SUCCESS = createActionType("GET_COMMENTS_SUCCESS");
export const GET_COMMENTS_ERROR = createActionType("GET_COMMENTS_ERROR");

export class GetComments implements Action {
  readonly type = GET_COMMENTS;
}

export class GetCommentsSuccess implements Action {
  readonly type = GET_COMMENTS_SUCCESS;
}

export class GetCommentsError implements Action {
  readonly type = GET_COMMENTS_ERROR;
}
