import { GET_COMMENTS_SUCCESS } from "./comments.actions";
const initialState = {};

export function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_COMMENTS_SUCCESS:
      return { ...state, comments: action["payload"] };
    default:
      return state;
  }
}
