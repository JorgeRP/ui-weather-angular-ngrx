import { ComponentsModule } from "./../../components/components.module";
import { ContactEffects } from "./state/contact.effects";
import { reducer } from "./state/contact.reducers";
import { environment } from "./../../environments/environment";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ContactRoutingModule } from "./contact-routing.module";
import { ContactComponent } from "./contact.component";
import { StoreModule } from "@ngrx/store";
import { StoreRouterConnectingModule } from "@ngrx/router-store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { EffectsModule } from "@ngrx/effects";
import { routerReducer } from "@ngrx/router-store";
import { HttpClientModule } from "@angular/common/http";
@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ContactRoutingModule,
    StoreModule.forFeature("contactStore", reducer),
    EffectsModule.forRoot([ContactEffects]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    ComponentsModule
  ],
  declarations: [ContactComponent]
})
export class ContactModule {}
