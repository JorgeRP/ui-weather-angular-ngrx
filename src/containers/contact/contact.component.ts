import { Component, OnInit, Input } from "@angular/core";
import { Store } from "@ngrx/store";
import { SendMessage } from "src/containers/contact/state/contact.actions";
import { createFeatureSelector, createSelector } from "@ngrx/store";

@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.css"]
})
export class ContactComponent implements OnInit {
  getContactStore = createFeatureSelector("contactStore");
  getContactData = createSelector(
    this.getContactStore,
    state => state["messageSent"]
  );
  data;
  messageSent;
  constructor(private store: Store<any>) {}

  ngOnInit() {
    this.data = this.store.select(this.getContactData);
    this.data.subscribe(data => {
      this.messageSent = data;
    });
  }

  postChange(event) {
    this.store.dispatch(new SendMessage(event));
  }
}
