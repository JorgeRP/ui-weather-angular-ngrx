import { SEND_MESSAGE_SUCCESS, SEND_MESSAGE_ERROR } from "./contact.actions";
const initialState = {
  messageSent: 0
};

export function reducer(state = initialState, action) {
  switch (action.type) {
    case SEND_MESSAGE_SUCCESS:
      return { ...state, messageSent: state.messageSent + 1 };
    case SEND_MESSAGE_ERROR:
      return {
        ...state,
        messageSent: false,
        messageSentError: action["payload"]
      };
    default:
      return state;
  }
}
