import { Action } from "@ngrx/store";
import { createActionType } from "src/shared/utils";

export const SEND_MESSAGE = createActionType("SEND_MESSAGE");
export const SEND_MESSAGE_SUCCESS = createActionType("SEND_MESSAGE_SUCCESS");
export const SEND_MESSAGE_ERROR = createActionType("SEND_MESSAGE_ERROR");

export class SendMessage implements Action {
  readonly type = SEND_MESSAGE;

  constructor(public payload) {}
}

export class SendMessageSuccess implements Action {
  readonly type = SEND_MESSAGE_SUCCESS;
}

export class SendMessageError implements Action {
  readonly type = SEND_MESSAGE_ERROR;
}
