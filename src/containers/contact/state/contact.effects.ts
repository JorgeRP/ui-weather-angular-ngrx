import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects/";
import { switchMap, map, catchError } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import {
  SEND_MESSAGE,
  SEND_MESSAGE_SUCCESS,
  SEND_MESSAGE_ERROR
} from "src/containers/contact/state/contact.actions";
import { of } from "rxjs/internal/observable/of";

@Injectable()
export class ContactEffects {
  URL_HEROKU_API = "http://reduxblog.herokuapp.com/";
  API_KEY = "afe3ee0c3173e1890785f28e032102a";
  ENDPOINTS = {
    POSTS: "api/posts"
  };
  constructor(private action$: Actions, private http: HttpClient) {}

  @Effect()
  post$ = this.action$.pipe(
    ofType(SEND_MESSAGE),
    switchMap(action => {
      return this.http
        .post(
          `${this.URL_HEROKU_API}${this.ENDPOINTS.POSTS}?key=${this.API_KEY}`,
          action["payload"]
        )
        .pipe(
          map(data => {
            return {
              payload: data,
              type: SEND_MESSAGE_SUCCESS
            };
          }),
          catchError(err => {
            return of({
              payload: err,
              type: SEND_MESSAGE_ERROR
            });
          })
        );
    })
  );
}
