import { ComponentsModule } from "./../components/components.module";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { RouterModule } from "@angular/router";
import { StoreModule } from "@ngrx/store";
import { StoreRouterConnectingModule } from "@ngrx/router-store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { EffectsModule } from "@ngrx/effects";
import { routerReducer } from "@ngrx/router-store";
import { environment } from "src/environments/environment";
import { reducer } from "src/app/state/app.reducers";
import { AppEffects } from "src/app/state/app.effects";

const routes = [
  {
    path: "",
    loadChildren: "../containers/weather/weather.module#WeatherModule"
  },
  {
    path: "contact",
    loadChildren: "../containers/contact/contact.module#ContactModule"
  },
  {
    path: "comments",
    loadChildren: "../containers/comments/comments.module#CommentsModule"
  }
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    ComponentsModule,
    StoreModule.forRoot({ reducer: reducer }),
    EffectsModule.forRoot([AppEffects]),
    StoreRouterConnectingModule.forRoot(),
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
