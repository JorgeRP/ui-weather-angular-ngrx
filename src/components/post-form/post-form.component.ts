import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  OnChanges,
  SimpleChanges
} from "@angular/core/src/metadata/lifecycle_hooks";

@Component({
  selector: "app-post-form",
  templateUrl: "./post-form.component.html",
  styleUrls: ["./post-form.component.css"]
})
export class PostFormComponent implements OnInit, OnChanges {
  form;
  formSent;
  @Output()
  post: EventEmitter<any> = new EventEmitter();
  @Input()
  messageSent;
  error;
  constructor() {}

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl("", Validators.required),
      email: new FormControl("", Validators.required),
      message: new FormControl("", Validators.required)
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.form) {
      if (this.messageSent) {
        this.form.reset();
        this.formSent = false;
      } else {
        this.error = true;
      }
    }
  }

  submit() {
    this.formSent = true;
    if (this.form.valid) {
      const post = {
        title: this.form.get("name").value,
        categories: this.form.get("email").value,
        content: this.form.get("message").value
      };

      this.post.emit(post);
    }
  }
}
