import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HumidityDetailComponent } from './humidity-detail.component';

describe('HumidityDetailComponent', () => {
  let component: HumidityDetailComponent;
  let fixture: ComponentFixture<HumidityDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HumidityDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HumidityDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
