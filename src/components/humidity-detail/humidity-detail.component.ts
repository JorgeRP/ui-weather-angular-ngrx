import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-humidity-detail",
  templateUrl: "./humidity-detail.component.html",
  styleUrls: ["./humidity-detail.component.css"]
})
export class HumidityDetailComponent implements OnInit {
  @Input()
  humidity;
  constructor() {}

  ngOnInit() {}
}
