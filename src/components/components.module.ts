import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ComponentsRoutingModule } from "./components-routing.module";
import { SearchBarComponent } from "./search-bar/search-bar.component";
import { DetailsComponent } from "./details/details.component";
import { PostFormComponent } from "./post-form/post-form.component";
import { CommentListComponent } from "./comment-list/comment-list.component";
import { HumidityDetailComponent } from "./humidity-detail/humidity-detail.component";
import { TemperatureDetailComponent } from "./temperature-detail/temperature-detail.component";
import { WindDetailComponent } from "./wind-detail/wind-detail.component";
import { ToolbarComponent } from "./toolbar/toolbar.component";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [CommonModule, ComponentsRoutingModule, ReactiveFormsModule],
  declarations: [
    SearchBarComponent,
    DetailsComponent,
    PostFormComponent,
    CommentListComponent,
    HumidityDetailComponent,
    TemperatureDetailComponent,
    WindDetailComponent,
    ToolbarComponent
  ],
  exports: [
    ToolbarComponent,
    SearchBarComponent,
    DetailsComponent,
    PostFormComponent,
    CommentListComponent
  ]
})
export class ComponentsModule {}
