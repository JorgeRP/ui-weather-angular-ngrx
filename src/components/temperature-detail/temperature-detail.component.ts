import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-temperature-detail",
  templateUrl: "./temperature-detail.component.html",
  styleUrls: ["./temperature-detail.component.css"]
})
export class TemperatureDetailComponent implements OnInit {
  @Input()
  current;
  @Input()
  min;
  @Input()
  max;

  constructor() {}

  ngOnInit() {}
}
