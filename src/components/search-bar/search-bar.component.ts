import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ViewChild } from "@angular/core";
import { ElementRef } from "@angular/core";

@Component({
  selector: "app-search-bar",
  templateUrl: "./search-bar.component.html",
  styleUrls: ["./search-bar.component.css"]
})
export class SearchBarComponent implements OnInit {
  form: FormGroup;
  @Output()
  search: EventEmitter<any> = new EventEmitter();
  @ViewChild("searchBar")
  searchBar: ElementRef;
  constructor() {}

  ngOnInit() {
    this.form = new FormGroup({
      search: new FormControl("", Validators.required)
    });
    this.searchBar.nativeElement.focus();
  }

  onSubmit() {
    const searchValue = this.form.get("search").value;
    this.search.emit(searchValue);
    this.form.reset();
  }
}
